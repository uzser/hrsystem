﻿using System;
using System.Diagnostics;
using System.ServiceModel;

namespace HRSystem.Service.Client
{
    public static class Service<TProxy>
    {
        private static readonly object SyncObj = new object();
        private static ChannelFactory<TProxy> _channelFactory;

        private static ChannelFactory<TProxy> ChannelFactory
        {
            get
            {
                if (_channelFactory == null)
                {
                    lock (SyncObj)
                    {
                        if (_channelFactory == null)
                            _channelFactory = new ChannelFactory<TProxy>(typeof(TProxy).Name);
                    }
                }
                return _channelFactory;
            }
        }

        public static TResult Use<TResult>(Func<TProxy, TResult> codeBlock)
        {
            using (var proxy = (IClientChannel)ChannelFactory.CreateChannel())
            {
                var success = false;
                try
                {
                    TResult result = codeBlock((TProxy)proxy);
                    proxy.Close();
                    success = true;
                    return result;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message, ex);
                    throw;
                }
                finally
                {
                    if (!success)
                    {
                        proxy.Abort();
                    }
                }
            }
        }

        public static void Use(Action<TProxy> codeBlock)
        {
            using (var proxy = (IClientChannel)ChannelFactory.CreateChannel())
            {
                bool success = false;
                try
                {
                    codeBlock((TProxy)proxy);
                    proxy.Close();
                    success = true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message, ex);
                    throw;
                }
                finally
                {
                    if (!success)
                    {
                        proxy.Abort();
                    }
                }
            }
        }
    }
}
