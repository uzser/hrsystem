﻿namespace HRSystem.Query.Request
{
    public class EmployeeRequest : EntityRequestBase
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }
    }
}
