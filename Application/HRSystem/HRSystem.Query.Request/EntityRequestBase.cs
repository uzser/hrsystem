﻿namespace HRSystem.Query.Request
{
    public class EntityRequestBase
    {
        public int? Id { get; set; }

        public int StartIndex { get; set; }

        public int? ResultCount { get; set; }
    }
}
