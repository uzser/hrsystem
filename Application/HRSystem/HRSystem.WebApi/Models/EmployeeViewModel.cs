﻿using System;

namespace HRSystem.WebApi.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }

        public int IndividualId { get; set; }

        public DateTime HireDate { get; set; }

        public DateTime? DismissalDate { get; set; }

        public int? DepartmentId { get; set; }

        public int? HeadEmployeeId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
