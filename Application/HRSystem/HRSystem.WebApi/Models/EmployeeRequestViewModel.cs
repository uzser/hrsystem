﻿namespace HRSystem.WebApi.Models
{
    public class EmployeeRequestViewModel
    {
        public int? Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }

        public int StartIndex { get; set; }

        public int? ResultCount { get; set; }
    }
}
