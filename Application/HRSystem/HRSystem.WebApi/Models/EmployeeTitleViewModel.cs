﻿using System;

namespace HRSystem.WebApi.Models
{
    public class EmployeeTitleViewModel
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public int EmployeeId { get; set; }

        public string TitleName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
