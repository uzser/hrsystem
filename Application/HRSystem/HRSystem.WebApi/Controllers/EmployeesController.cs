﻿using AutoMapper;
using HRSystem.Entity;
using HRSystem.Query.Request;
using HRSystem.Service.Client;
using HRSystem.Service.Contract;
using HRSystem.WebApi.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace HRSystem.WebApi.Controllers
{
    public class EmployeesController : ApiController
    {
        // GET: api/employees/5
        public EmployeeViewModel Get(int id)
        {
            var employee = Service<IQueryService>.Use(svc => svc.GetEmployees(new EmployeeRequest { Id = id })).Rows.Single();

            return Mapper.Map<EmployeeViewModel>(employee);
        }

        // GET: api/employees/search?firstName=John&secondName=Smith
        [HttpGet]
        public IEnumerable<EmployeeViewModel> Search([FromUri]EmployeeRequestViewModel request)
        {
            var serviceRequest = Mapper.Map<EmployeeRequest>(request);
            var employees = Service<IQueryService>.Use(svc => svc.GetEmployees(serviceRequest)).Rows;

            return Mapper.Map<IEnumerable<vEmployee>, IEnumerable<EmployeeViewModel>>(employees);
        }

        // GET: api/employees/5/titles
        [HttpGet]
        [Route("api/employees/{employeeId}/titles")]
        public IEnumerable<EmployeeTitleViewModel> GetTitles(int employeeId)
        {
            var employeeTitles = Service<IQueryService>.Use(svc => svc.GetEmployeeTitles(employeeId));
            return Mapper.Map<IEnumerable<vEmployeeTitle>, IEnumerable<EmployeeTitleViewModel>>(employeeTitles);
        }

        // GET: api/employees/5/head
        [HttpGet]
        [Route("api/employees/{employeeId}/head")]
        public EmployeeViewModel GetHeadEmployee(int employeeId)
        {
            var headEmployee = Service<IQueryService>.Use(svc => svc.GetHeadEmployee(employeeId));
            return Mapper.Map<EmployeeViewModel>(headEmployee);
        }
    }
}
