﻿using Newtonsoft.Json.Serialization;
using System.Web;
using System.Web.Http;

namespace HRSystem.WebApi
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            // Autofac
            AutoMapperConfiguration.Configure();
            // JSONFormat: js naming convention
            GlobalConfiguration.Configuration
               .Formatters
               .JsonFormatter
               .SerializerSettings
               .ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
