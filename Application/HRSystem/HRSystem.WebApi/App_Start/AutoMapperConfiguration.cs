﻿using AutoMapper;
using HRSystem.Entity;
using HRSystem.Query.Request;
using HRSystem.WebApi.Models;

namespace HRSystem.WebApi
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<vEmployee, EmployeeViewModel>();
                cfg.CreateMap<vEmployeeTitle, EmployeeTitleViewModel>();
                cfg.CreateMap<EmployeeRequestViewModel, EmployeeRequest>();
            });
        }
    }
}
