using HRSystem.Data.Mapping;
using HRSystem.Entity;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace HRSystem.Data
{
    public class HRSystemDbContext : DbContext
    {
        static HRSystemDbContext()
        {
            Database.SetInitializer<HRSystemDbContext>(null);
        }

        public HRSystemDbContext()
            : base("Name=HRSystemDbContext")
        {
        }

        public ObjectContext ObjectContext
        {
            get { return ((IObjectContextAdapter)this).ObjectContext; }
        }

        public DbSet<DBVersion> DBVersions { get; set; }

        public DbSet<Department> Departments { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<EmployeeTitle> EmployeeTitles { get; set; }

        public DbSet<Individual> Individuals { get; set; }

        public DbSet<Title> Titles { get; set; }

        public DbSet<vEmployee> vEmployees { get; set; }

        public DbSet<vEmployeeTitle> vEmployeeTitles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new DBVersionMap());
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new EmployeeTitleMap());
            modelBuilder.Configurations.Add(new IndividualMap());
            modelBuilder.Configurations.Add(new TitleMap());
            modelBuilder.Entity<vEmployee>();
            modelBuilder.Entity<vEmployeeTitle>();
        }
    }
}
