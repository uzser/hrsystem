﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using HRSystem.Entity.Common;

namespace HRSystem.Data.Repositories
{
    public interface IBaseRepository<TEntity>
        where TEntity : IEntityBase
    {
        IList<TEntity> GetAll();

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where);
    }
}