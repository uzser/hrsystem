﻿using HRSystem.Entity.Common;
using HRSystem.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace HRSystem.Data.Repositories
{
    public class QueryRepository<T> : BaseRepository<T>, IQueryRepository<T>
        where T : class, IEntityBase, new()
    {
        #region .ctor
        public QueryRepository(ObjectContext objectContext)
            : base(objectContext)
        { }
        #endregion

        private IQueryable<T> _currentQuery;

        protected IQueryable<T> CurrentEntityQuery;

        protected virtual IQueryable<T> CurrentQuery
        {
            get { return _currentQuery ?? (_currentQuery = ObjectSet.AsQueryable()); }
            set
            {
                _currentQuery = value;
            }
        }

        public virtual void Flush()
        {
            CurrentEntityQuery = null;
            _currentQuery = null;
        }

        public void AddFilter(Expression<Func<T, bool>> where)
        {
            CurrentQuery = CurrentQuery.Where(where);
        }

        public void Skip(int count)
        {
            CurrentQuery = CurrentQuery.Skip(() => count);
        }

        public void Take(int count)
        {
            CurrentQuery = CurrentQuery.Take(() => count);
        }

        public IList<T> ExecuteCurrentQuery()
        {
            return CurrentQuery.AsNoTracking().ToList();
        }

        public void Include<TProperty>(Expression<Func<T, TProperty>> exp)
        {
            if (CurrentEntityQuery == null)
                CurrentEntityQuery = CurrentQuery;
            CurrentQuery = CurrentQuery.Include(exp);
        }

        public virtual PaginatedResult<T> GetPage(int startIndex, int? resultCount)
        {
            int rowCount = CurrentQuery.Count();
            if (rowCount == 0)
                return new PaginatedResult<T>(rowCount, Enumerable.Empty<T>());
            if (startIndex > 0)
                CurrentQuery = CurrentQuery.Skip(() => startIndex);
            if (resultCount.HasValue)
                CurrentQuery = CurrentQuery.Take(() => resultCount.Value);

            var list = CurrentQuery.AsNoTracking().ToList();
            return new PaginatedResult<T>(rowCount, list);
        }
    }
}