﻿using HRSystem.Entity.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace HRSystem.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class, IEntityBase
    {
        protected IObjectSet<TEntity> ObjectSet { get; private set; }

        protected ObjectContext ObjectContext { get; private set; }

        #region .ctor
        public BaseRepository(ObjectContext objectContext)
        {
            ObjectSet = objectContext.CreateObjectSet<TEntity>();
            ObjectContext = objectContext;
        }
        #endregion

        #region IBaseRepository<T>
        public IList<TEntity> GetAll()
        {
            return ObjectSet.ToArray();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where)
        {
            return ObjectSet.Where(where).ToArray();
        }

        #endregion
    }
}