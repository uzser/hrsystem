﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using HRSystem.Entity.Common;
using HRSystem.Infrastructure.Common;

namespace HRSystem.Data.Repositories
{
    public interface IQueryRepository<T> : IBaseRepository<T> where T : class, IEntityBase
    {
        void AddFilter(Expression<Func<T, bool>> where);

        void Flush();

        void Include<TProperty>(Expression<Func<T, TProperty>> exp);

        void Skip(int count);

        void Take(int count);

        IList<T> ExecuteCurrentQuery();

        PaginatedResult<T> GetPage(int startIndex, int? resultCount);
    }
}