using HRSystem.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HRSystem.Data.Mapping
{
    public class EmployeeTitleMap : EntityTypeConfiguration<EmployeeTitle>
    {
        public EmployeeTitleMap()
        {
            // Primary Key
            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Relationships
            HasRequired(t => t.Employee)
                .WithMany(t => t.EmployeeTitles)
                .HasForeignKey(d => d.EmployeeId);

            HasRequired(t => t.Title)
                .WithMany(t => t.EmployeeTitles)
                .HasForeignKey(d => d.TitleId);
        }
    }
}
