using HRSystem.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HRSystem.Data.Mapping
{
    public class DBVersionMap : EntityTypeConfiguration<DBVersion>
    {
        public DBVersionMap()
        {
            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.DBVersionNumber).HasColumnName("DBVersion");
        }
    }
}
