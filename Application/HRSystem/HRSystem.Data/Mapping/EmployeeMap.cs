using HRSystem.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HRSystem.Data.Mapping
{
    public class EmployeeMap : EntityTypeConfiguration<Employee>
    {
        public EmployeeMap()
        {
            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Relationships
            HasOptional(t => t.Department)
                .WithMany(t => t.Employees)
                .HasForeignKey(d => d.DepartmentId);

            HasOptional(t => t.HeadEmployee)
                .WithMany(t => t.SubordinateEmployees)
                .HasForeignKey(d => d.HeadEmployeeId);

            HasRequired(t => t.Individual)
                .WithMany(t => t.Employees)
                .HasForeignKey(d => d.IndividualId);
        }
    }
}
