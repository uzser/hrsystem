using HRSystem.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace HRSystem.Data.Mapping
{
    public class IndividualMap : EntityTypeConfiguration<Individual>
    {
        public IndividualMap()
        {
            HasKey(t => t.Id);
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(255);

            Property(t => t.MiddleName)
                .HasMaxLength(255);

            Property(t => t.SecondName)
                .IsRequired()
                .HasMaxLength(255);
        }
    }
}
