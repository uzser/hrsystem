﻿using HRSystem.Entity;
using HRSystem.Infrastructure.Common;
using HRSystem.Query.Request;
using System.ServiceModel;

namespace HRSystem.Service.Contract
{
    public partial interface IQueryService : IEmployeeQueryService { }

    [ServiceContract]
    public interface IEmployeeQueryService
    {
        [OperationContract]
        vEmployee GetHeadEmployee(int employeeId);

        [OperationContract]
        PaginatedResult<vEmployee> GetEmployees(EmployeeRequest request);

        [OperationContract]
        PaginatedResult<vEmployeeTitle> GetEmployeeTitles(int employeeId);
    }
}
