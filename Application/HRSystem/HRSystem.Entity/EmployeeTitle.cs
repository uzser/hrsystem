using HRSystem.Entity.Common;
using System;

namespace HRSystem.Entity
{
    public class EmployeeTitle : IEntityBase
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public int EmployeeId { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Title Title { get; set; }
    }
}
