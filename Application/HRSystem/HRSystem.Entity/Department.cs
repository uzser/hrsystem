using HRSystem.Entity.Common;
using System.Collections.Generic;

namespace HRSystem.Entity
{
    public class Department : IEntityBase
    {
        public Department()
        {
            Employees = new List<Employee>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Employee> Employees { get; private set; }
    }
}
