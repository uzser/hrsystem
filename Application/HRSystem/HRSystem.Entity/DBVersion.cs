using HRSystem.Entity.Common;

namespace HRSystem.Entity
{
    public class DBVersion : IEntityBase
    {
        public int Id { get; set; }

        public decimal DBVersionNumber { get; set; }

        public string Comments { get; set; }
    }
}
