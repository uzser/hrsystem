using HRSystem.Entity.Common;
using System;
using System.Collections.Generic;

namespace HRSystem.Entity
{
    public class Employee : IEntityBase
    {
        public Employee()
        {
            SubordinateEmployees = new List<Employee>();
            EmployeeTitles = new List<EmployeeTitle>();
        }

        public int Id { get; set; }

        public int IndividualId { get; set; }

        public DateTime HireDate { get; set; }

        public DateTime? DismissalDate { get; set; }

        public int? DepartmentId { get; set; }

        public int? HeadEmployeeId { get; set; }

        public virtual Department Department { get; set; }

        public virtual Employee HeadEmployee { get; set; }

        public virtual Individual Individual { get; set; }

        public virtual ICollection<EmployeeTitle> EmployeeTitles { get; private set; }

        public virtual ICollection<Employee> SubordinateEmployees { get; private set; }
    }
}
