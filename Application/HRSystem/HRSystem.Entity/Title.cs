using HRSystem.Entity.Common;
using System.Collections.Generic;

namespace HRSystem.Entity
{
    public class Title : IEntityBase
    {
        public Title()
        {
            this.EmployeeTitles = new List<EmployeeTitle>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<EmployeeTitle> EmployeeTitles { get; private set; }
    }
}
