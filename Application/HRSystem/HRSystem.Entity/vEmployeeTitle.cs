﻿using HRSystem.Entity.Common;
using System;

namespace HRSystem.Entity
{
    public class vEmployeeTitle : IEntityBase
    {
        public int Id { get; set; }

        public int TitleId { get; set; }

        public int EmployeeId { get; set; }

        public string TitleName { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}
