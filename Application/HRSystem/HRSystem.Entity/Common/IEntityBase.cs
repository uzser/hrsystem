﻿namespace HRSystem.Entity.Common
{
    public interface IEntityBase
    {
        int Id { get; set; }
    }
}
