using HRSystem.Entity.Common;
using System;
using System.Collections.Generic;

namespace HRSystem.Entity
{
    public class Individual : IEntityBase
    {
        public Individual()
        {
            Employees = new List<Employee>();
        }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string SecondName { get; set; }

        public DateTime BirthDate { get; set; }

        public virtual ICollection<Employee> Employees { get; private set; }
    }
}
