﻿using HRSystem.Data.Repositories;
using HRSystem.Entity;
using HRSystem.Infrastructure.Common;
using HRSystem.Query.Request;

namespace HRSystem.Query
{
    public class EmployeeQuery : QueryBase<EmployeeRequest, PaginatedResult<vEmployee>>
    {
        public IQueryRepository<vEmployee> QueryRepository { get; set; }

        public EmployeeQuery(IQueryRepository<vEmployee> repository)
        {
            QueryRepository = repository;
        }

        public override PaginatedResult<vEmployee> ExecuteQuery(EmployeeRequest request)
        {
            if (request.Id.HasValue)
                QueryRepository.AddFilter(x => x.Id == request.Id.Value);

            if (request.FirstName != null)
                QueryRepository.AddFilter(x => x.FirstName.Contains(request.FirstName));

            if (request.MiddleName != null)
                QueryRepository.AddFilter(x => x.MiddleName.Contains(request.MiddleName));

            if (request.SecondName != null)
                QueryRepository.AddFilter(x => x.SecondName.Contains(request.SecondName));

            return QueryRepository.GetPage(request.StartIndex, request.ResultCount);
        }
    }
}
