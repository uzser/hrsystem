﻿using HRSystem.Data.Repositories;
using HRSystem.Entity;
using HRSystem.Infrastructure.Common;

namespace HRSystem.Query
{
    public class EmployeeTitleQuery : QueryBase<int, PaginatedResult<vEmployeeTitle>>
    {
        public IQueryRepository<vEmployeeTitle> QueryRepository { get; set; }

        public EmployeeTitleQuery(IQueryRepository<vEmployeeTitle> repository)
        {
            QueryRepository = repository;
        }

        public override PaginatedResult<vEmployeeTitle> ExecuteQuery(int employeeId)
        {
            QueryRepository.AddFilter(x => x.EmployeeId == employeeId);
            return QueryRepository.GetPage(0, int.MaxValue);
        }
    }
}
