﻿namespace HRSystem.Query
{
    public abstract class QueryBase<TRequest, TResult>
    {
        public abstract TResult ExecuteQuery(TRequest request);
    }
}
