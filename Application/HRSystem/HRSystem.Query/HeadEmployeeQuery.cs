﻿using HRSystem.Data.Repositories;
using HRSystem.Entity;
using System.Linq;

namespace HRSystem.Query
{
    public class HeadEmployeeQuery : QueryBase<int, vEmployee>
    {
        public IQueryRepository<vEmployee> QueryRepository { get; set; }

        public HeadEmployeeQuery(IQueryRepository<vEmployee> repository)
        {
            QueryRepository = repository;
        }

        public override vEmployee ExecuteQuery(int employeeId)
        {
            QueryRepository.AddFilter(x => x.Id == employeeId);
            var headEmployeeId = QueryRepository.ExecuteCurrentQuery().Single().HeadEmployeeId;
            QueryRepository.Flush();

            QueryRepository.AddFilter(x => x.Id == headEmployeeId);
            return QueryRepository.ExecuteCurrentQuery().Single();
        }
    }
}
