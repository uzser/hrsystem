﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace HRSystem.Infrastructure.Common
{
    [Serializable]
    public class PaginatedResult<T> : IEnumerable<T>
    {
        public PaginatedResult(int count, IEnumerable<T> rows)
        {
            Count = count;
            Rows = rows;
        }

        public PaginatedResult()
        { }

        public int Count { get; private set; }

        public IEnumerable<T> Rows { get; private set; }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return Rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Rows.GetEnumerator();
        }
    }
}
