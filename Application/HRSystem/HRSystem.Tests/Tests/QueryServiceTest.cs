using HRSystem.Query.Request;
using HRSystem.Service.Client;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HRSystem.Tests.Tests
{
    [TestClass]
    public class QueryServiceTest
    {
        [TestMethod]
        public void QueryServiceGetEmployees()
        {
            var employees = Service<Service.Contract.IQueryService>.Use(svc => svc.GetEmployees(new EmployeeRequest { Id = 2 }));
            Assert.IsNotNull(employees);
            Assert.IsTrue(employees.Count == 1);
        }
    }
}