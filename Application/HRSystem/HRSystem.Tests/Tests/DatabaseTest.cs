using HRSystem.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace HRSystem.Tests.Tests
{
    [TestClass]
    public class DatabaseTest
    {
        [TestMethod]
        public void DbGetData()
        {
            using (var context = new HRSystemDbContext())
            {
                var employees = context.Employees.Take(5).ToArray();
                var subEmployees = employees.SelectMany(x => x.SubordinateEmployees).ToArray();
                var headEmployee = employees.Select(x => x.HeadEmployee).ToArray();

                Assert.IsTrue(employees.Any(), "Employees are not found");
                Assert.IsTrue(subEmployees.Any(), "Subordinates are not found");
                Assert.IsTrue(headEmployee.Any(), "Heads are not found");
            }
        }
        [TestMethod]
        public void DbGetEmployeeTitles()
        {
            using (var context = new HRSystemDbContext())
            {
                var employeeTitles = context.vEmployeeTitles.Take(2).ToArray();

                Assert.IsTrue(employeeTitles.Any(), "Employee titles are not found");
            }
        }
    }
}