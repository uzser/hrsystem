﻿using HRSystem.WebApi;
using HRSystem.WebApi.Controllers;
using HRSystem.WebApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace HRSystem.Tests.Tests
{
    [TestClass]
    public class WebApiTest
    {
        [TestMethod]
        public void WebApiGetEmployee()
        {
            var controller = new EmployeesController();
            var employeeViewModel = controller.Get(2);
            Assert.IsNotNull(employeeViewModel);
            Assert.IsTrue(employeeViewModel.FirstName == "Петр");
        }

        [TestMethod]
        public void WebApiSearchEmployees()
        {
            var controller = new EmployeesController();
            var employeeViewModels = controller.Search(new EmployeeRequestViewModel { FirstName = "Иван" });
            Assert.IsNotNull(employeeViewModels);

            Assert.IsTrue(employeeViewModels.Count() == 1);

            var employeeViewModel = employeeViewModels.Single();
            Assert.IsTrue(employeeViewModel.FirstName == "Иван");
        }

        [TestMethod]
        public void WebApiGetTitles()
        {
            var controller = new EmployeesController();
            var employeeTitleViewModels = controller.GetTitles(5);
            Assert.IsNotNull(employeeTitleViewModels);

            Assert.IsTrue(employeeTitleViewModels.Count() == 2);

            Assert.IsTrue(employeeTitleViewModels.Any(x => x.TitleName == "Бухгалтер"));
            Assert.IsTrue(employeeTitleViewModels.Any(x => x.TitleName == "Главный бухгалтер"));
        }

        [TestMethod]
        public void WebApiGetHead()
        {
            var controller = new EmployeesController();
            var headEmployeeViewModel = controller.GetHeadEmployee(2);
            Assert.IsNotNull(headEmployeeViewModel);

            Assert.IsTrue(headEmployeeViewModel.FirstName == "Иван");
        }

        [ClassInitialize]
        public static void WebApiTestConfigure(TestContext context)
        {
            AutoMapperConfiguration.Configure();
        }
    }
}
