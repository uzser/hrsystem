﻿using Autofac;
using HRSystem.Data;
using HRSystem.Data.Repositories;
using HRSystem.Entity;
using HRSystem.Query;
using HRSystem.Service.Contract;

namespace HRSystem.Service
{
    /// <summary>
    /// Class to build Autofac IOC container.
    /// </summary>
    public static class AutofacContainerBuilder
    {
        /// <summary>
        /// Configures and builds Autofac IOC container.
        /// </summary>
        /// <returns></returns>
        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            //// register types
            builder.RegisterType<QueryService.QueryService>().As<IQueryService>();

            builder.Register<IQueryRepository<vEmployee>>(
                c => new QueryRepository<vEmployee>(new HRSystemDbContext().ObjectContext));

            builder.Register<IQueryRepository<vEmployeeTitle>>(
                c => new QueryRepository<vEmployeeTitle>(new HRSystemDbContext().ObjectContext));

            builder.RegisterType<EmployeeQuery>();
            builder.RegisterType<HeadEmployeeQuery>();
            builder.RegisterType<EmployeeTitleQuery>();

            // build container
            return builder.Build();
        }
    }
}
