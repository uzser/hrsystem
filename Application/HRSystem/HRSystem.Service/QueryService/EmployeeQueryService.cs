﻿using Autofac;
using HRSystem.Entity;
using HRSystem.Infrastructure.Common;
using HRSystem.Query;
using HRSystem.Query.Request;

namespace HRSystem.Service.QueryService
{
    public partial class QueryService
    {
        public ILifetimeScope Scope { get; set; }

        public QueryService(ILifetimeScope scope)
        {
            Scope = scope;
        }

        public vEmployee GetHeadEmployee(int employeeId)
        {
            return Scope.Resolve<HeadEmployeeQuery>().ExecuteQuery(employeeId);
        }

        public PaginatedResult<vEmployee> GetEmployees(EmployeeRequest request)
        {
            return Scope.Resolve<EmployeeQuery>().ExecuteQuery(request);
        }

        public PaginatedResult<vEmployeeTitle> GetEmployeeTitles(int employeeId)
        {
            return Scope.Resolve<EmployeeTitleQuery>().ExecuteQuery(employeeId);
        }
    }
}
