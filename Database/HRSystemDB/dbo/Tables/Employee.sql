﻿CREATE TABLE [dbo].[Employee]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY CLUSTERED ([Id] ASC),
	[IndividualId] INT NOT NULL, 
	[HireDate] DATE NOT NULL, 
	[DismissalDate] DATE NULL, 
	[DepartmentId] INT NULL, 
	[HeadEmployeeId] INT NULL, 
	CONSTRAINT [FK_Employee_Individual] FOREIGN KEY ([IndividualId]) REFERENCES [Individual]([Id]), 
	CONSTRAINT [FK_Employee_Department] FOREIGN KEY ([DepartmentId]) REFERENCES [Department]([Id]), 
	CONSTRAINT [FK_Employee_HeadEmployee] FOREIGN KEY ([HeadEmployeeId]) REFERENCES [Employee]([Id])
)
