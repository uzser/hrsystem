﻿CREATE TABLE [dbo].[EmployeeTitle]
(
	[Id] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY CLUSTERED ([Id] ASC),
	[TitleId] INT NOT NULL, 
	[EmployeeId] INT NOT NULL, 
	[FromDate] DATE NOT NULL, 
	[ToDate] DATE NULL, 
	CONSTRAINT [FK_EmployeeTitle_Title] FOREIGN KEY ([TitleId]) REFERENCES [Title]([Id]),
	CONSTRAINT [FK_EmployeeTitle_Employee] FOREIGN KEY ([EmployeeId]) REFERENCES [Employee]([Id])
)
