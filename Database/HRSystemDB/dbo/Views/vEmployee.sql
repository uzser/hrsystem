﻿CREATE VIEW [dbo].[vEmployee]
AS SELECT
	E.Id
	,E.IndividualId
	,E.HireDate
	,E.DismissalDate
	,E.DepartmentId
	,E.HeadEmployeeId
	
	,I.FirstName
	,I.MiddleName
	,I.SecondName
	,I.BirthDate
FROM Employee E
JOIN Individual I ON I.Id = E.IndividualId
LEFT JOIN Employee HeadE ON HeadE.Id = E.HeadEmployeeId
LEFT JOIN Individual HeadI ON HeadI.Id = HeadE.IndividualId

