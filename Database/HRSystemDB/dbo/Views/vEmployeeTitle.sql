﻿CREATE VIEW [dbo].[vEmployeeTitle]
AS SELECT
	ET.Id
	,ET.EmployeeId
	,ET.TitleId
	,ET.FromDate
	,ET.ToDate
	,T.Name AS TitleName
FROM EmployeeTitle ET
JOIN Title T ON ET.TitleId = T.Id

