﻿SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

SET LOCK_TIMEOUT 30000
GO

BEGIN TRY
    BEGIN TRANSACTION

    BEGIN
        :r .\Scripts\PostIncremental.sql
    END

    COMMIT

END TRY
BEGIN CATCH
    DECLARE @error_message NVARCHAR(4000);
    DECLARE @error_severity INT;
    DECLARE @error_state INT;

    SELECT @error_message = ERROR_MESSAGE() + ' (Line: ' + CAST(ERROR_LINE() as nvarchar(5)) + ')',
           @error_severity = ERROR_SEVERITY(),
           @error_state = ERROR_STATE();

    IF @@TRANCOUNT > 0 
        ROLLBACK;

    RAISERROR (@error_message, @error_severity, @error_state);
END CATCH
GO
