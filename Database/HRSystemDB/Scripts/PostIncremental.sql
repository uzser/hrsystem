﻿PRINT N'=== PostIncremental ==='

DECLARE @DBVersion DECIMAL(7,3),
		@Comments NVARCHAR(MAX)

SET @DBVersion = 1.001
SET @Comments = N'Init script'
IF NOT EXISTS (SELECT [DBVersion] FROM [dbo].[DBVersion] WHERE [DBVersion]=@DBVersion)
BEGIN
	PRINT N'[' + CAST(@DBVersion AS NVARCHAR(8)) + N']. ' + @Comments
	
	-- Department
	SET IDENTITY_INSERT dbo.Department ON

	INSERT INTO Department(Id, Name)
	VALUES
		(1,  N'Отдел бухгалтерии')
		,(2, N'Отдел информационных технологий')

	SET IDENTITY_INSERT dbo.Department OFF
	
	-- Title
	SET IDENTITY_INSERT dbo.Title ON

	INSERT INTO Title(Id, Name)
	VALUES
		(1,  N'Генеральный директор')
		,(2, N'Начальник отдела')
		,(3, N'Системный администратор')
		,(4, N'Главный бухгалтер')
		,(5, N'Бухгалтер')

	SET IDENTITY_INSERT dbo.Title OFF

	INSERT INTO [dbo].[DBVersion] ([DBVersion], [Comments]) VALUES (@DBVersion, @Comments)
END

SET @DBVersion = 1.002
SET @Comments = N'Test data'
IF NOT EXISTS (SELECT [DBVersion] FROM [dbo].[DBVersion] WHERE [DBVersion]=@DBVersion)
BEGIN
	PRINT N'[' + CAST(@DBVersion AS NVARCHAR(8)) + N']. ' + @Comments

	-- Individual
	SET IDENTITY_INSERT dbo.Individual ON

	INSERT INTO Individual (Id, SecondName, FirstName, MiddleName, BirthDate)
	VALUES
		(1, N'Иванов', N'Иван', N'Иванович', '1970-06-01')
		,(2, N'Петров', N'Петр', N'Петрович', '1980-07-01')
		,(3, N'Васильев', N'Василий', N'Васильевич', '1975-07-01')
		,(4, N'Кузнецова', N'Анна', N'Павловна', '1980-08-01')

	SET IDENTITY_INSERT dbo.Individual OFF
	
	-- Employee
	SET IDENTITY_INSERT dbo.Employee ON

	INSERT INTO Employee(Id, IndividualId, HireDate, DismissalDate, DepartmentId, HeadEmployeeId)
	VALUES
		(1, 1, '2014-01-01', NULL, NULL, NULL)
		,(2, 2, '2014-01-01', NULL, 2, 1)
		,(3, 3, '2014-01-01', '2014-05-01', 2, 2)
		,(4, 3, '2015-01-01', NULL, 2, 2)
		,(5, 4, '2014-01-01', NULL, 1, 1)

	SET IDENTITY_INSERT dbo.Employee OFF
	
	-- EmployeeTitle
	INSERT INTO EmployeeTitle(EmployeeId, TitleId, FromDate, ToDate)
	VALUES
		(1, 1, '2014-01-01', NULL)
		,(2, 2, '2014-01-01', NULL)
		,(3, 3, '2014-01-01', NULL)
		,(4, 3, '2015-01-01', NULL)
		,(5, 5, '2014-01-01', '2014-03-01')
		,(5, 4, '2014-03-01', NULL)

	INSERT INTO [dbo].[DBVersion] ([DBVersion], [Comments]) VALUES (@DBVersion, @Comments)
END